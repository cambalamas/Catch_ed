﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeAreaSound : MonoBehaviour
{
    public GameObject CorrectAgent;

    private void OnTriggerEnter(Collider other)
    {
        if (CorrectAgent.transform.GetComponent<Collider>() == other)
        {
            Audio.I.Play("SafeArea");
        }
    }

}
